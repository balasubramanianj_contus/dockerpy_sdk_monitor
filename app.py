import docker

def load_container(client):
	data=client.containers.list()
	for cont in data:
		print(cont.stats(decode=True, stream=False))
		print(cont.name)
def load_service(client):
	data=client.services.list()
	print(data)
def load_network(client):
	data=client.networks.list()
	print(data)
def load_volume(client):
	data=client.volumes.list()
	print(data)

# client = docker.from_env()
client=docker.DockerClient(base_url='192.168.4.224:2376')
node_list=client.nodes.list()

for node in node_list:
	role = (node.attrs['Spec']['Role'])
	print(role,"============")
	node_ip = node.attrs['Status']['Addr']
	node_client=docker.DockerClient(base_url=node_ip+':2376')

	# if role=='manager':
	# 	load_service(node_client)
	
	load_container(node_client)
	# load_volume(node_client)